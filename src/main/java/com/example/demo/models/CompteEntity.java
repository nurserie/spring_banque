package com.example.demo.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_compte_cpt")
public class CompteEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cpt_id;
	
	private Double cpt_solde;
	private Double cpt_maxDecouvert;
	private Double cpt_tauxEpargne;
	@OneToMany(mappedBy = "opr_id")
	private List<OperationEntity> cpt_listOperation;
	@ManyToOne
	private TypeCompteEntity cpt_typeCompte;
	

	public CompteEntity() {

	}


	public CompteEntity(int cpt_id, Double cpt_solde, Double cpt_maxDecouvert, Double cpt_tauxEpargne,
			List<OperationEntity> cpt_listOperation, TypeCompteEntity cpt_typeCompte) {
		super();
		this.cpt_id = cpt_id;
		this.cpt_solde = cpt_solde;
		this.cpt_maxDecouvert = cpt_maxDecouvert;
		this.cpt_tauxEpargne = cpt_tauxEpargne;
		this.cpt_listOperation = cpt_listOperation;
		this.cpt_typeCompte = cpt_typeCompte;
	}


	public int getCpt_id() {
		return cpt_id;
	}


	public void setCpt_id(int cpt_id) {
		this.cpt_id = cpt_id;
	}


	public Double getCpt_solde() {
		return cpt_solde;
	}


	public void setCpt_solde(Double cpt_solde) {
		this.cpt_solde = cpt_solde;
	}


	public Double getCpt_maxDecouvert() {
		return cpt_maxDecouvert;
	}


	public void setCpt_maxDecouvert(Double cpt_maxDecouvert) {
		this.cpt_maxDecouvert = cpt_maxDecouvert;
	}


	public Double getCpt_tauxEpargne() {
		return cpt_tauxEpargne;
	}


	public void setCpt_tauxEpargne(Double cpt_tauxEpargne) {
		this.cpt_tauxEpargne = cpt_tauxEpargne;
	}


	public List<OperationEntity> getCpt_listOperation() {
		return cpt_listOperation;
	}


	public void setCpt_listOperation(List<OperationEntity> cpt_listOperation) {
		this.cpt_listOperation = cpt_listOperation;
	}


	public TypeCompteEntity getCpt_typeCompte() {
		return cpt_typeCompte;
	}


	public void setCpt_typeCompte(TypeCompteEntity cpt_typeCompte) {
		this.cpt_typeCompte = cpt_typeCompte;
	}
	
	
}
