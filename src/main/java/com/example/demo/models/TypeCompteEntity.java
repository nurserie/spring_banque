package com.example.demo.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_r_typecompte_tcp")
public class TypeCompteEntity {
	@Id
	private int tcp_id;
	private String tcp_libelle;
	@OneToMany(mappedBy = "cpt_id")
	private List<CompteEntity> tcp_listCompte;
	
	public TypeCompteEntity() {

	}

	public TypeCompteEntity(int tcp_id, String tcp_libelle, List<CompteEntity> tcp_listCompte) {
		super();
		this.tcp_id = tcp_id;
		this.tcp_libelle = tcp_libelle;
		this.tcp_listCompte = tcp_listCompte;
	}

	public int getTcp_id() {
		return tcp_id;
	}

	public void setTcp_id(int tcp_id) {
		this.tcp_id = tcp_id;
	}

	public String getTcp_libelle() {
		return tcp_libelle;
	}

	public void setTcp_libelle(String tcp_libelle) {
		this.tcp_libelle = tcp_libelle;
	}

	public List<CompteEntity> getTcp_listCompte() {
		return tcp_listCompte;
	}

	public void setTcp_listCompte(List<CompteEntity> tcp_listCompte) {
		this.tcp_listCompte = tcp_listCompte;
	}
	
	
}
