package com.example.demo.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_operation_opr")
public class OperationEntity {
	@Id
	private int opr_id;
	private double opr_montant;
	private Date opr_date;
	@ManyToOne
	private CompteEntity opr_compte;
	@ManyToOne
	private TypeOperationEntity opr_typeCompte;
	@OneToOne
	private OperationEntity opr_operation;
	
	public OperationEntity() {

	}

	public OperationEntity(int opr_id, Double opr_montant, Date opr_date, CompteEntity opr_compte,
			TypeOperationEntity opr_typeCompte, OperationEntity opr_operation) {
		super();
		this.opr_id = opr_id;
		this.opr_montant = opr_montant;
		this.opr_date = opr_date;
		this.opr_compte = opr_compte;
		this.opr_typeCompte = opr_typeCompte;
		this.opr_operation = opr_operation;
	}

	public int getOpr_id() {
		return opr_id;
	}

	public void setOpr_id(int opr_id) {
		this.opr_id = opr_id;
	}

	public Double getOpr_montant() {
		return opr_montant;
	}

	public void setOpr_montant(Double opr_montant) {
		this.opr_montant = opr_montant;
	}

	public Date getOpr_date() {
		return opr_date;
	}

	public void setOpr_date(Date opr_date) {
		this.opr_date = opr_date;
	}

	public CompteEntity getOpr_compte() {
		return opr_compte;
	}

	public void setOpr_compte(CompteEntity opr_compte) {
		this.opr_compte = opr_compte;
	}

	public TypeOperationEntity getOpr_typeCompte() {
		return opr_typeCompte;
	}

	public void setOpr_typeCompte(TypeOperationEntity opr_typeCompte) {
		this.opr_typeCompte = opr_typeCompte;
	}

	public OperationEntity getOpr_operation() {
		return opr_operation;
	}

	public void setOpr_operation(OperationEntity opr_operation) {
		this.opr_operation = opr_operation;
	}
	
	
}
