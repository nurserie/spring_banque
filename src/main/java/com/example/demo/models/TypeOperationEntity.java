package com.example.demo.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_r_typeoperation_top")
public class TypeOperationEntity {
	
	@Id
	private int tcp_id;
	private String tcp_libelle;
	@OneToMany(mappedBy = "opr_id")
	private List<OperationEntity> tcp_listOperation;
	
	public TypeOperationEntity() {
		// TODO Auto-generated constructor stub
	}

	public TypeOperationEntity(int tcp_id, String tcp_libelle, List<OperationEntity> tcp_listOperation) {
		super();
		this.tcp_id = tcp_id;
		this.tcp_libelle = tcp_libelle;
		this.tcp_listOperation = tcp_listOperation;
	}

	public int getTcp_id() {
		return tcp_id;
	}

	public void setTcp_id(int tcp_id) {
		this.tcp_id = tcp_id;
	}

	public String getTcp_libelle() {
		return tcp_libelle;
	}

	public void setTcp_libelle(String tcp_libelle) {
		this.tcp_libelle = tcp_libelle;
	}

	public List<OperationEntity> getTcp_listOperation() {
		return tcp_listOperation;
	}

	public void setTcp_listOperation(List<OperationEntity> tcp_listOperation) {
		this.tcp_listOperation = tcp_listOperation;
	}
	
}
