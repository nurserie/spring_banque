package com.example.demo.dto;

import com.example.demo.models.TypeCompteEntity;

public class CompteDto {
	private Double cpt_solde;
	private Double cpt_maxDecouvert;
	private Double cpt_tauxEpargne;
	private TypeCompteEntity cpt_typeCompte;
	
	public CompteDto() {
		// TODO Auto-generated constructor stub
	}

	public CompteDto(Double cpt_solde, Double cpt_maxDecouvert, Double cpt_tauxEpargne,
			TypeCompteEntity cpt_typeCompte) {
		super();
		this.cpt_solde = cpt_solde;
		this.cpt_maxDecouvert = cpt_maxDecouvert;
		this.cpt_tauxEpargne = cpt_tauxEpargne;
		this.cpt_typeCompte = cpt_typeCompte;
	}

	public Double getCpt_solde() {
		return cpt_solde;
	}

	public void setCpt_solde(Double cpt_solde) {
		this.cpt_solde = cpt_solde;
	}

	public Double getCpt_maxDecouvert() {
		return cpt_maxDecouvert;
	}

	public void setCpt_maxDecouvert(Double cpt_maxDecouvert) {
		this.cpt_maxDecouvert = cpt_maxDecouvert;
	}

	public Double getCpt_tauxEpargne() {
		return cpt_tauxEpargne;
	}

	public void setCpt_tauxEpargne(Double cpt_tauxEpargne) {
		this.cpt_tauxEpargne = cpt_tauxEpargne;
	}

	public TypeCompteEntity getCpt_typeCompte() {
		return cpt_typeCompte;
	}

	public void setCpt_typeCompte(TypeCompteEntity cpt_typeCompte) {
		this.cpt_typeCompte = cpt_typeCompte;
	}
	
	
}
