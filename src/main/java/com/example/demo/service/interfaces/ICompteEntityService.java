package com.example.demo.service.interfaces;

import com.example.demo.dto.CompteDto;

public interface ICompteEntityService {
	public CompteDto getCompte(int id);
}
