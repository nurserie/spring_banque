package com.example.demo.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CompteDto;
import com.example.demo.models.CompteEntity;
import com.example.demo.repository.ICompteRepository;
import com.example.demo.service.interfaces.ICompteEntityService;

@Service
public class CompteEntityServiceImpl implements ICompteEntityService{
 
	@Autowired private ICompteRepository compteRepository;
	
	private ModelMapper modelMapper;

	@Override
	public CompteDto getCompte(int id) {
		CompteEntity compteEntity
		return modelMapper.map(CompteEntity, CompteDto.class);
	}
}
